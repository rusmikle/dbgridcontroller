**You don't have to replace your TDBGrid object!**
--

You need more functionalities? A grid controller that provide extra features to your existing TDBGrid 

- Searching expression in the grid
- Searching expression in column
- Column filter editor
- Datetime editor, memo editor and lookup editor
- Saving the current filter view (JSON)
- Multi column sorting
- Column chooser editor
- Column grouping visual separator on one level
- No data indicator
- Footer and aggregation on columns
- Perform 50000 records under a second


**Documentation**

https://wiki.freepascal.org/TdxDBGridController

